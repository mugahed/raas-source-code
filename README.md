# RaaS source code

This repo contains the source code for RaaS Paper, the code is open5gs commit number (f0206c79f619f8e3c091918aeb8623a07f2a13b6) + modification of the smf seletion function(smf_sess_select_upf) within the /src/smf/context.c file. 

To use it:

1. Clone project:
   - ```sudo git clone https://gitlab.flux.utah.edu/mugahed/raas-source-code.git /var/tmp/raas```
2. Decompress:
   - ```sudo mkdir /var/tmp/open5gs && sudo tar -xvzf community_images.tar.gz -C /var/tmp/open5gs && sudo mv /var/tmp/open5gs/open5gs_smf_slct_fun/* /var/tmp/open5gs/```
3. Compile. using:
    - ```sudo meson build --prefix=`pwd`/install```
    - ```sudo ninja -C build install```